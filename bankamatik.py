#Bankamatik Uygulaması

GultenHesap = {
    'ad' : 'Gülten Deregözü',
    'hesapNo' : '44323442',
    'bakiye' : 4560,
    'ekHesap' : 2340
}

AhmetHesap = {
    'ad': 'Ahmet Aydın',
    'hesapNo': '55443398',
    'bakiye': 8600,
    'ekHesap': 1200
}

def paraCek(hesap, miktar):
    print(f"Merhaba {hesap['ad']}")

    if (hesap['bakiye'] >= miktar):
        hesap['bakiye'] -= miktar
        print('paranızı alabilirsiniz')
        bakiyeSorgula(hesap)
    else:
        toplam = hesap['bakiye'] + hesap['ekHesap']

        if (toplam >= miktar):
            ekHesapKullanimi =  input('ek hesap kullanılsın mı (e/h)')   

            if ekHesapKullanimi == 'e':
                
                ekHesapKullanilacakMiktar = miktar - hesap['bakiye']
                hesap['bakiye'] = 0
                hesap['ekHesap'] -= ekHesapKullanilacakMiktar
                print('paranızı alabilirsiniz')
                bakiyeSorgula(hesap)
            else:
                print(f"{hesap['hesapNo']} nolu hesabınızda {hesap['bakiye']} bulunmaktadır")
        else:
            print('üzgünüz bakiye yetersiz')       
            bakiyeSorgula(hesap)



def bakiyeSorgula(hesap):
    print(f"{hesap['hesapNo']} nolu hesabınıda {hesap['bakiye']} TL bulunmaktadır. Ek hesap limitiniz ise {hesap['ekHesap']} TL dir.")                 

paraCek(GultenHesap, 4000)
print('>>--------------------<<')
paraCek(GultenHesap, 2000)
